<?php
echo "<h3>Question 1 Answer is :</h3> <br> ";
$a=array("A","B","C","A","B");
print_r(array_count_values($a));
echo "<br><br>";

echo "<h3>Question 2 Answer is : </h3><br> ";
$a=array("a"=>"Volvo","b"=>"BMW","c"=>"Toyota");
print_r(array_reverse($a));
echo "<br><br>";

echo "<h3>Question 3 Answer is :</h3> <br> ";
$a=array("a"=>"red","b"=>"green","c"=>"blue");
echo array_search("red",$a);
echo "<br><br>";

echo "<h3>Question 4 Answer is : </h3><br> ";
$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("a"=>"purple","b"=>"orange");
array_splice($a1,0,2,$a2);
print_r($a1);
echo "<br><br>";

echo "<h3>Question 5 Answer is :</h3> <br> ";
$a=array("a"=>"red","b"=>"green");
array_unshift($a,"blue");
print_r($a);
echo "<br><br>";

echo "<h3>Question 6 Answer is :</h3> <br> ";
$a=array("Name"=>"Peter","Age"=>"41","Country"=>"USA");
print_r(array_values($a));
echo "<br><br>";

echo "<h3>Question 7 Answer is : </h3><br> ";
$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("e"=>"red","f"=>"green","g"=>"blue");
$result=array_diff($a1,$a2);
print_r($result);
echo "<br><br>";

echo "<h3>Question 8 Answer is :</h3> <br> ";
function test_odd($var)
{
    return($var & 1);
}
$a1=array("a","b",2,3,4);
print_r(array_filter($a1,"test_odd"));
echo "<br><br>";

echo "<h3>Question 9 Answer is :</h3> <br> ";
$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$result=array_flip($a1);
print_r($result);
echo "<br><br>";

echo "<h3>Question 10 Answer is : </h3><br> ";
$a=array("Volvo"=>"XC90","BMW"=>"X5","Toyota"=>"Highlander");
print_r(array_keys($a));
echo "<br><br>";

echo "<h3>Question 11 Answer is : </h3><br> ";
$a1=array("red","green");
$a2=array("blue","yellow");
print_r(array_merge($a1,$a2));
echo "<br><br>";

echo "<h3>Question 12 Answer is : </h3><br> ";
$a=array("red","green");
array_push($a,"blue","yellow");
print_r($a);
echo "<br><br>";

echo "<h3>Question 13 Answer is :</h3> <br> ";
$a=array("red","green","blue","yellow","brown");
$random_keys=array_rand($a,3);
echo $a[$random_keys[0]]."<br>";
echo $a[$random_keys[1]]."<br>";
echo $a[$random_keys[2]];
echo "<br><br>";

echo "<h3>Question 14 Answer is : </h3><br> ";
$a1=array("red","green");
$a2=array("blue","yellow");
print_r(array_replace($a1,$a2));
echo "<br><br>";

echo "<h3>Question 15 Answer is :</h3> <br> ";
var_dump((bool) "false");
echo "<br><br>";

echo "<h3>Question 16 Answer is : </h3><br> ";
$value = '1234.10abc ';
echo intval($value);
echo "<br><br>";

echo "<h3>Question 17 Answer is : </h3><br> ";
$foo = "1abc"; $bar = true;
echo settype($foo, "integer");
echo settype($bar, "string");
echo "<br><br>";

echo "<h3>Question 1 Answer is :</h3> <br> ";
class StrValTest
{
    public function __toString()
    {
        return __CLASS__;
    }
}
echo strval(new StrValTest);
echo "<br><br>";

echo "<h3>Question 18 Answer is :</h3> <br> ";
$var1 = 'Hello World';
$var2 = '';
$var2 =& $var1;
debug_zval_dump($var1);
echo "<br><br>";

class User {
    public $firstName;
    public $lastName;

    public function hello()
    {
        return "hello";
    }
}

echo "<h3>But Correct Answer is :- string(11) \"Hello World\" refcount(2) </h3>";

echo " is_bool() <br><br>";

echo "<br><br>";